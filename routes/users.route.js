const express = require("express");
const router = express.Router();

const mongoose = require("mongoose");
const UserModel = require("../models/user.model");

const Cryptr = require('cryptr');
const cryptr = new Cryptr('migzzat');
/******************************/

/*---[GET /users]---*/
router.get("/" , (req, res, next) => {
      UserModel.find()
         .exec()
         .then(results => {
            res.status(200).json(results);
         })
         .catch(error => {
            res.status(500).json({
                error,
            });
         });
});

/*---[POST /users]---*/
router.post("/" , (req, res, next) => {
    console.log(req.body);
    const User = new UserModel({
        _id      : new mongoose.Types.ObjectId(),
        fname    : req.body.fname,
        lname    : req.body.lname,
        uname    : req.body.uname,
        email    : req.body.email,
        password : cryptr.encrypt(req.body.password)
    });
    
    User.save().then(results => {
        console.log(results);
        res.status(201).json({
            done    :  true,
            msg     :  "User was created",
            error   :  "",
            results,
       });
    }).catch(error => {
        res.status(500).json({
            done  :  false,
            msg   :  "error :(",
            error ,
            results:{}
       });
    });
});

/*---[GET /users/:userID]---*/
router.get("/:userID" , (req, res, next) => {
    console.log(req.params.userID)
       UserModel.findById(req.params.userID)
          .exec()
          .then(doc => {
              console.log(doc)
              if(doc){
                res.status(200).json({
                    done    : true,
                    msg     : "success :)",
                    error   : "",
                    results : doc
                  });
              }else{
                res.status(200).json({
                    done    : false,
                    msg     : "not data found",
                    error   : "",
                    results : {}
                  });
              }
          })
          .catch(error => {
              res.status(500).json({
                    done    : false,
                    msg     : "error :(",
                    error,
                    results : {}
              });
          });
});

/*---[PATCH /users/:userID]---*/
router.patch("/:userId", (req, res, next)=>{
    const userId = req.params.userId;
    const upOps = {};
 
    for(const ops of req.body){
         upOps[ops.propName] = ops.value;
    }

    console.log(upOps)
    UserModel.update({_id : userId} , {$set:upOps})
      .exec()
      .then(results => {
          console.log(results);
          res.status(200).json({
             done    : true,
             msg     : "succsess",
             error   : "",
             results,
          });
      }).catch(error => {
         res.status(500).json({
             done    : false,
             msg     : "error",
             error,
             results : {}
          });
      });
 });

/*---[DELETE /users/:userID]---*/
router.delete("/:userId", (req, res, next)=>{
   UserModel.deleteOne({_id : req.params.userId})
     .exec()
     .then(results => {
         console.log(results);
         res.status(200).json({
            done    : true,
            msg     : "succsess",
            error   : "",
            results,
         });
     }).catch(error => {
        res.status(500).json({
            done    : false,
            msg     : "error",
            error,
            results : {}
         });
     });
});
/******************************/
module.exports = router;