const mongoose = require("mongoose");

const UserSchema = mongoose.Schema({
    _id      : mongoose.Schema.Types.ObjectId,
    fname    : String ,
    lname    : String ,
    uname    : String ,
    email    : String ,
    password : String
});

module.exports = mongoose.model("Users", UserSchema);