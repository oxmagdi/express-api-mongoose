require("custom-env").env("config")
const PORT = process.env.PROJECT_PORT;
const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const mongoose = require("mongoose");
/****************/

/*-------[connect mongoose db]-------*/
mongoose.connect(`mongodb+srv://migzz:${ process.env.MONGO_ATLAS_PW }@node-c1-qkrq4.mongodb.net/test?retryWrites=true`,{
    useNewUrlParser : true
});

/*-------[Middlewares]-------*/
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
// parse application/json
app.use(bodyParser.json())

/*-------[routes]-------*/
app.use("/users" , require("./routes/users.route"));

/****************/
app.listen(PORT , ()=>{
    console.log(`app start on ${ PORT }`);
});